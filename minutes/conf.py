# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Sphinx book'
copyright = '2022, Yoshimasa Obayashi'
author = 'Yoshimasa Obayashi'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

#extensions = ['sphinxjp.themes.basicstrap']
extensions = []

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

language = 'ja'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

#html_theme = 'alabaster'
#html_theme = 'classic'
#html_theme = 'bizstyle'
#html_theme = 'bootstrap'
#html_theme_path = sphinx_bootstrap_theme.get_html_theme_path()
#html_theme = 'basicstrap'
#html_theme_options = {
#  'googlewebfont': True,
#  'googlewebfont_url': 'http://fonts.googleapis.com/css?family=NotoSans',
#  'googlewebfont_style': "font-family: 'Noto Sans', sans-serif",
#  # Set the Size of Heading text. Defaults to None
#  'h1_size': '3.0em',
#  'h2_size': '2.6em',
#  'h3_size': '2.2em',
#  'h4_size': '1.8em',
#  'h5_size': '1.4em',
#  'h6_size': '1.1em',
#}

html_theme = 'sphinx_rtd_theme'

#html_static_path = ['_static']
